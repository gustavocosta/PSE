#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>



int main(void)
{
    clock_t Ticks[2];
    int acc[8];
    int qtd[8];
    float ave;
    int index;
    float result;

    //f1 without optimization
    Ticks[0] = clock();
    for(int i = 0; i < 10000; i++)
    {
    	memset(acc, (int)0, sizeof(int)*8);
    	memset(qtd, (int)0, sizeof(int)*8);
    	ave = 0;
    	result = 0;
    	for(int j = 0; j < 80; j++)
    	{
    		acc[j%8] = acc[j%8] + rand();
    		qtd[j%8]++;
    	}

    	ave = (acc[0]+acc[1]+acc[2]+acc[3]+acc[4]+acc[5]+acc[6]+acc[7]+acc[8])/
    	(qtd[0]+qtd[1]+qtd[2]+qtd[3]+qtd[4]+qtd[5]+qtd[6]+qtd[7]+qtd[8]);

    	ave = ave*3.14159/180;

    	result = cos(ave);

    	//printf("Ave: %0.4f; Result: %0.4f\n", ave, result);
    }
    Ticks[1] = clock();
    printf("Result: %f\n", result);
    double time1 = (Ticks[1] - Ticks[0]);

    //f2 with optimization
    Ticks[0] = clock();
    for(int i = 10000; i; --i)
    {
    	memset(acc, (int)0, 32);
    	memset(qtd, (int)0, 32);
    	ave = 0;
    	result = 0;
    	for(int j = 80; j; --j)
    	{
    		index = (80 - j) & 7; //(80-j)%8
    		acc[index] = acc[index] + rand();
    		++qtd[index];
    	}

    	ave = (acc[0]+acc[1]+acc[2]+acc[3]+acc[4]+acc[5]+acc[6]+acc[7]+acc[8])/
    	(qtd[0]+qtd[1]+qtd[2]+qtd[3]+qtd[4]+qtd[5]+qtd[6]+qtd[7]+qtd[8]);

    	ave = ave*0.0174532925; //degrees to rad

    	result = 1 - ave*ave*(0.5 + ave*ave*(0.041666 + 
    		ave*ave*(0.001388 + ave*ave/40320.0)));

    	//printf("Ave: %0.4f; Result: %0.4f\n", ave, result);
    }
    Ticks[1] = clock();
    printf("Result: %f\n", result);
    double time2 = (Ticks[1] - Ticks[0]);

    printf("time1: %g CLKs; time2: %g CLKs\n", time1, time2);
    printf("%0.2f%%\n", 100-time2*100.0/time1);
    return 0;
}