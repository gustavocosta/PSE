#ifndef _COMMUNICATION_H
#define _COMMUNICATION_H

#include <stdint.h>

static void (*_write)(uint8_t);
static void (*_read)(uint8_t*);

typedef enum
{
	slop = 0x0120,
	tap  = 0x0130,
	stap = 0x0140,
	x    = 0x0150,
	y    = 0x0160,
	z    = 0x0170
}Address;

typedef enum
{
	read,
	write
}Operation;

typedef struct
{
	union{
		uint16_t full;
		struct{
			uint8_t low;
			uint8_t high;
        }bytes;
	}add;
	uint8_t operation;
	uint8_t data;
} Message;

void addCommand(Message *m, Address *a, Operation *o, uint8_t *data);

void sendCommand(Message *m);

void driverInit(void (*write)(uint8_t), void (*read)(uint8_t*));

#endif
