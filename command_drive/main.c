#include <stdio.h>
#include "communication.h"
#include "i2c.h"

int main()
{
    Message m;
    Address a = tap;
    Operation o = write;
    uint8_t data = 4;

    driverInit(i2c_write, i2c_read);

    addCommand(&m, &a, &o, &data);

    sendCommand(&m);

    a = x;
    o = read;
    data = 0;

    addCommand(&m, &a, &o, &data);

    sendCommand(&m);

    return 0;
}
