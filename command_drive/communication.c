#include "communication.h"

void addCommand(Message *m, Address *a, Operation *o, uint8_t *data)
{
    m->add.full = *a;
    m->operation = *o;
    m->data = *data;
}

void sendCommand(Message *m)
{
    (*_write)(m->add.bytes.low);
    (*_write)(m->add.bytes.high);
    (*_write)(m->operation);
    (*_write)(m->data);
    if(m->operation == read){
        (*_read)(m->data);
    }
}

void driverInit(void (*write)(uint8_t), void (*read)(uint8_t*))
{
    _write = write;
    _read = read;
}
