TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.c \
    communication.c \
    i2c.c

HEADERS += \
    communication.h \
    i2c.h
