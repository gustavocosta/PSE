#ifndef __BUFFER_CIRC_H
#define __BUFFER_CIRC_H

#include <stdint.h>
#include <stdio.h>

typedef struct{
    uint8_t *data;
    uint8_t *head;
    uint8_t *tail;
    int size;
} circular_buffer;

void buffer_init(circular_buffer *cb, uint8_t *data, int size);

void buffer_insert(circular_buffer *cb, uint8_t data);

int buffer_full(circular_buffer *cb);

#endif
