#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "buffer_circ.h"

int main(){

    circular_buffer cb;
    uint8_t data[80];
    buffer_init(&cb, data, 80);
    uint8_t val;

    for(int i = 0; i < 81; i++)
    {
        val = (uint8_t) rand();
        printf("Hex %d: %x \n", i, val);
        buffer_insert(&cb, val);
        sleep(0.1);
    }

    printf("\n====================\n\n");

    int array[20] = {0};
    memcpy(array, cb.data, sizeof(array));

    for(int i = 0; i < 20; i++)
    {
       printf("Int %d: %d \n", i, array[i]);
    }
}
