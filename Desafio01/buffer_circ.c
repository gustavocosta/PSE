#include "buffer_circ.h"

void buffer_init(circular_buffer *cb, uint8_t *data,int size)
{
    cb->data = data;
    cb->head = cb->data;
    cb->tail = 0;
    cb->size = size;
}

void buffer_insert(circular_buffer *cb, uint8_t data)
{
    if(!buffer_full(cb))
    {
        if(cb->tail == 0){
            cb->tail = cb->data;
        }

        *(cb->tail) = data;
        cb->tail = cb->tail + 1;

        if(cb->tail == (cb->data + cb->size))
        {
            cb->tail = cb->data;
        }
    }
}

int buffer_full(circular_buffer *cb)
{
    int status;
    if(cb->head == cb->tail)
    {
        printf("Buffer full!\n");
        status = 1;
    }
    else
    {
        status = 0;
    }

    return status;
}
